# ASPHYXPERT

**AsphyXpert** is an expert system mainly dealing with asphyxiate disease. 

## Installation
- Create a mysql database and give it a name of `asphyxpert`, or you can give any name but make sure to change the database name in `app/config/database.php`
- Open cmd (windows) or terminal and navigate to the root folder of the project and type `composer update` (make sure you already install [composer](http://getcomposer.org)) 
- After composer has finished manage your dependency run `php artisan migrate` to fill your database with necessary table
- Run `php artisan db:seed` to populate the data
- For testing purposes use email of `fikri.desertlion@gmail.com` and password `123456`

## Team Members
- Rijalul Fikri
- Diah Sekarsari R
- Sulvia T.
- Elizabeth P. G
- Khairunnisa