<?php
Blade::setContentTags('<<', '>>'); 		// for variables and all things Blade
Blade::setEscapedContentTags('<<<', '>>>'); 	// for escaped data

Route::get('/', array('as'=>'home','uses'=>'UserController@getLogin')); // login form
Route::post('login', array('as'=>'login','uses'=>'UserController@postLogin')); // proses login
Route::get('logout', array('as'=>'logout','uses'=>'UserController@getLogout')); // proses login

// semua route yang berada di grup ini harus login dulu untuk mengaksesnya
Route::group(['before'=>'auth'],function()
{
	Route::get('rules', array('as'=>'rule.index','uses'=>'RuleController@getIndex'));
	Route::get('test', array('as'=>'test','uses'=>'RuleController@getTest'));
	Route::get('ruleitems', array('as'=>'ruleitem.index','uses'=>'RuleitemController@getIndex'));
	Route::get('diagnose', array('as'=>'diagnose.index','uses'=>'DiagnoseController@getIndex'));
	Route::group(['prefix'=>'api/v1'],function()
	{
		// get all ruleitems
		Route::get('ruleitems', array('as'=>'ruleitem.all','uses'=>'RuleitemController@getAll'));
		// get one ruleitem
		Route::get('ruleitem/{id}', array('as'=>'ruleitem.find','uses'=>'RuleitemController@getFind'));
		// store ruleitem
		Route::post('ruleitem/store', array('as'=>'ruleitem.store','uses'=>'RuleitemController@postStore'));
		// delete ruleitem
		Route::get('ruleitem/{id}/delete', array('as'=>'ruleitem.delete','uses'=>'RuleitemController@getDelete'));
		// load all rule
		Route::get('rules', array('as'=>'rule.all','uses'=>'RuleController@getAll'));
		// store rule
		Route::post('rule/store', array('as'=>'rule.store','uses'=>'RuleController@postStore'));
		// Ambil symptom dari rule
		Route::get('rule/{id}/symptom',['as'=>'rule.get.symptom','uses'=>'RuleController@getFindSymptom']);
		// Diagnose the Disease
		Route::post('diagnose', array('as'=>'diagnose.api','uses'=>'DiagnoseController@postDiagnose'));
	});
});