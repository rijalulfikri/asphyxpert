<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Welcome to ASPHYXPERT &raquo; Please Login</title>
	<style>
		*, *:before, *:after {
			-webkit-box-sizing: border-box;
			-moz-box-sizing: border-box;
			box-sizing: border-box;
			margin: 0 auto; padding: 0;
		}
		html {
			width: 100%; height: 100%;
		}
		body {
			font-family: sans-serif;
			background-image: url('<< asset('img/login-bckg.jpg') >>');
			background-repeat: no-repeat;
			background-position: bottom right;
			background-size: cover;
			min-width: 100%; min-height: 100%;
			position: relative;
		}
		#login-form {
			position: absolute;
			bottom: 140px;
			right: 183px;
			background: transparent url('<< asset('img/login-logo.png')>>') no-repeat left top;
			width: 500px;
			height: 217px;
			padding-left: 250px;
		}
		form {
			background: #FFF;
			-webkit-box-shadow: 0 0 15px rgba(0,0,0,.9);
			-moz-box-shadow: 0 0 15px rgba(0,0,0,.9);
			box-shadow: 0 0 15px rgba(0,0,0,.9);
			-webkit-border-radius: 5px;
			-moz-border-radius: 5px;
			border-radius: 5px;
		}
		input {
			display: block; width: 100%;
			border: none;
			line-height: 2.3em;
			padding: 5px 7px;
			font-size: .7em;
			outline: none;
			text-align: left;
		}
		input[type=text] {
			-webkit-border-radius: 5px 5px 0 0;
			-moz-border-radius: 5px 5px 0 0;
			border-radius: 5px 5px 0 0;
			border-bottom: 1px solid rgba(0,0,0,.2);
		}
		input[type=submit] {
			-webkit-border-radius: 0 0 5px 5px;
			-moz-border-radius: 0 0 5px 5px;
			border-radius: 0 0 5px 5px;
			cursor: pointer;
			background: #36b797;
			border-top: 1px solid #95d9c8;
			-webkit-box-shadow: 0 -2px 3px rgba(0,0,0,.2);
			-moz-box-shadow: 0 -2px 3px rgba(0,0,0,.2);
			box-shadow: 0 -2px 3px rgba(0,0,0,.2);
			color: #eed8a4;
			text-transform: uppercase;
			text-shadow: 0 -1px 0 rgba(0,0,0,.2);
			text-align: center;
			font-weight: bold;
		}
		input[type=submit]:hover {
			background: #38c5a2;
		}
		img { display: block; margin: 15px auto;}
		button {
			position: absolute;
			top: 10px;
			right: 10px;
			border: none;
			border-radius: 50%;
			padding: 3px 7px;
			background: rgba(255,255,255,.3);
		}
		.alert {
			position: fixed;
			top: 0;
			left: 0;
			width: 100%;
			background: rgba(0,0,0,.5);
			padding: 10px;
			color: #FFF;
			line-height: 1.3em;
		}
	</style>
</head>
<body>
	@include('layouts.notification')
	<div id="container">
		<div id="login-form">
			<img src="<< asset('img/pls-login.png') >>" alt="Please Login">
			<< Form::open(['route'=>'login']) >>
				<< Form::text('email','',['placeholder'=>'Email','required'=>'required']) >>
				<< Form::password('password',['placeholder'=>'Password','required'=>'required'])>>
				<< Form::submit('login') >>
			<< Form::close() >>
		</div>
	</div>
</body>
</html>