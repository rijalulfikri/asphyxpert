@section('content')
	<div ng-controller="diagnoseController as diag">
		<h1>Diagnose Disease</h1>
		<hr>
		<h3>Choose Symptoms and Click Diagnose</h3>
		<hr>
		<div class="input-group">
			<input type="text" placeholder="Filter through the symptoms" ng-model="ruleitemSearch" class="form-control">
			<span class="input-group-addon"><i class="fa fa-search"></i></span>
		</div>
		<hr>
		<ul class="block-grid">
			<li ng-repeat="r in ruleitems | filter:ruleitemSearch" class="repeat-item">
				<label><input type="checkbox" ng-model="inputs[$index].checked"> {{ r.name }}</label>
				<input type="text" placeholder="CF value" ng-disabled="!inputs[$index].checked" ng-model="inputs[$index].cf">
			</li>
		</ul>
		<button class="btn btn-primary btn-block" ng-click="diagnose()">
			<i class="fa fa-stethoscope"></i> Diagnose
		</button>
		<div id="results" ng-show="isResult" class="repeat-item">
			<hr>
			<div id="chartdiv" style="width:100%; height: 300px; background: #444;">
			</div>
			<hr>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>Penyakit</th>
						<th>Gejala</th>
						<th>CF</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="k in data">
						<td>{{ symptomlist[k.id] }}</td>
						<td>
							<div ng-repeat="g in k.symptoms">
								<span>{{ symptomlist[g.ruleitem_id] }}<br></span>
							</div>
						</td>
						<td>{{ k.cf }}</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="text-center" ng-show="!isResult">
			<h1>Diagnosing ...</h1>
		</div>
	</div>
@stop