@section('content')
	<div ng-controller="ruleitemController">
		<h2>
			Manage Rule Items
		</h2>
		<form role="form" class="form-horizontal form-add">
			<fieldset>
				<legend>Rule Item Form <button class="btn btn-add" ng-click="isCollapsed = !isCollapsed"><i class="fa fa-chevron-down"></i></button></legend>
				<div collapse="isCollapsed">
					<div class="form-group">
						<label for="ruleitem-name" class="control-label col-xs-4">Rule Item Name</label>
						<div class="col-xs-8">
							<input type="text" id="ruleitem-name" placeholder="Type Rule Item Name" ng-model="ruleitem.name" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-8 col-xs-offset-4">
							<button class="btn btn-danger" ng-click="ruleitem.resetState()" ng-show="ruleitem.editState"><i class="fa fa-times"></i> Cancel Editing</button>
							<button type="submit" class="btn btn-primary" ng-click="ruleitem.storeRuleitem()"><i class="fa fa-save"></i> Save</button>
						</div>
					</div>
				</div>
			</fieldset>
		</form>
		<hr>
		<form role="form" class="form-horizontal">
			<div class="form-group">
				<div class="col-xs-12">
					<div class="input-group">
						<input type="text" placeholder="Filter rule item by name" ng-model="ruleitemSearch" class="form-control">
						<span class="input-group-addon"><i class="fa fa-search"></i></span>
					</div>
				</div>
			</div>
		</form>
		<ul class="item-list">
			<li ng-repeat="ruleitem in ruleitems | filter: ruleitemSearch" class="repeat-item">
				{{ ruleitem.name }}
				<div class="btn-group btn-group-xs">
					<a href="#" ng-click="editRuleitem(ruleitem.id,ruleitem.name,$index)" class="btn btn-primary"><i class="fa fa-edit"></i> Edit</a>
					<a href="#" ng-click="deleteRuleitem(ruleitem.id,$index)" class="btn btn-danger"><i class="fa fa-trash-o"></i> Delete</a>
				</div>
			</li>
		</ul>
	</div>
@stop()