<!doctype html>
<html lang="en" ng-app="asphyxpert">
<head>
	<meta charset="UTF-8">
	<title>ASPHYXPERT</title>
	<!-- CSS mulai disini -->
	<link rel="stylesheet" href="<< asset('css/style.css') >>">
	<script>var baseUrl = '<< route('home') >>'+'/api/v1';</script>
	<script>var baseUri = '<< route('home') >>';</script>
</head>
<body>
	<section class="header">
		<header>
			<h1><a href="<< route('ruleitem.index') >>">ASPHYXPERT</a></h1>
			<nav>
				<ul>
					<li>
						<a href="<< route('ruleitem.index') >>"<< (Request::is('ruleitem*')) ? ' class="active"' : '' >>><i class="fa fa-ambulance"></i> Rule Items</a>
					</li>
					<li>
						<a href="<< route('rule.index') >>"<< (Request::is('rules')) ? ' class="active"' : '' >>><i class="fa fa-medkit"></i> Rules</a>
					</li>
					<li>
						<a href="<< route('diagnose.index') >>"<< (Request::is('diagnose*')) ? ' class="active"' : '' >>><i class="fa fa-stethoscope"></i> Diagnose</a>
					</li>
				</ul>
			</nav>
		</header>
	</section>
	<div id="container">
		@include('layouts.notification')
		@yield('content')
	</div>
	<script src="<< asset('js/main.js') >>"></script>
</body>
</html>