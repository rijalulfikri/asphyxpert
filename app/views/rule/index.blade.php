@section('content')
	<div ng-controller="ruleController">
		<h2>
			Manage Rules
		</h2>
		<form role="form" class="form-horizontal form-add">
			<fieldset>
				<legend>Manage Rule Form <button class="btn btn-add" ng-click="isCollapsed = !isCollapsed"><i class="fa fa-chevron-down"></i></button></legend>
				<div collapse="isCollapsed">
					<div class="form-group">
						<label for="rule-name" class="control-label col-xs-4">Rule</label>
						<div class="col-xs-8">
							<div class="row">
								<div class="col-xs-9">
									<input type="text" ng-model="rule.name" typeahead="r as r.name for r in ruleitems.symptoms | filter:$viewValue | limitTo:8" class="form-control" placeholder="Start typing to see suggestion">
								</div>
								<div class="col-xs-3">
									<input type="text" ng-model="rule.cf" placeholder="CF value" class="form-control">
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-4">Symptoms</label>
						<div class="col-xs-8">
							<div class="input-group">
								<input type="text" ng-model="findsymptom" placeholder="Type symptom name to filter" class="form-control">
								<div class="input-group-addon"><i class="fa fa-search"></i></div>
							</div>
							<div class="checkbox">
								<ul class="block-grid">
									<li ng-repeat="s in ruleitems.symptoms | filter: findsymptom">
										<label><input type="checkbox" value="" ng-model="rule.symptoms[$index].checked" name="symptoms[]"> {{ s.name }}</label>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-8 col-xs-offset-4"><button type="submit" class="btn-primary btn-block" ng-click="rule.storeRule()">Save</button></div>
					</div>
				</div>
			</fieldset>
		</form>
		<hr>
		<form role="form" class="form-horizontal">
			<div class="form-group">
				<div class="col-xs-12">
					<div class="input-group">
					<input type="text" placeholder="Filter rule by name" ng-model="ruleSearch" class="form-control">
						<div class="input-group-addon"><i class="fa fa-search"></i></div>
					</div>
				</div>
			</div>
		</form>
		<ul class="item-list">
			<li ng-repeat="r in rulelists | filter: rSearch">
				<p ng-click="showSymptoms(r.id)">{{ r.name }}</p>
				<div class="btn-group btn-group-xs">
					<a href="#" ng-click="editRuleitem(r.id,r.name,$index)" class="btn btn-primary"><i class="fa fa-edit"></i> Edit</a>
					<a href="#" ng-click="deleteRuleitem(r.id,$index)" class="btn btn-danger"><i class="fa fa-trash-o"></i> Delete</a>
				</div>
			</li>
		</ul>
	</div>
@stop()