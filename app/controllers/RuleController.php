<?php
class RuleController extends BaseController {

	// layout utama yang bakal digunakan untuk seluruh view
	protected $layout = 'layouts.main';

	public function getIndex()
	{
		$this->layout->content = View::make('rule.index');
	}

	public function getTest(){
		return Rule::with('Ruleitem')->find(2);
		// dd(Rule::find(2));
		// dd(array_key_exists(15, [['id'=>1,'cf'=>0.5],['id'=>2,'cf'=>0.5],['id'=>15,'cf'=>0.5]]));
	}

	// api untuk ambil seluruh penyakit
	public function getAll(){
		return Rule::with('ruleitem')->get(['id','ruleitem_id']);
	}

	// api untuk ambil satu penyakit berdasarkan id
	public function getFind($id){
		return Rule::find($id);
	}

	public function getFindSymptom($id){
		return Symptom::with('ruleitem')->where('rule_id','=',$id)->get();
	}

	// api untuk menyimpan data symptom
	public function postStore()
	{
		// ambil inputnya
		$input = Input::only(['id','name','cf','symptoms']);
		$symptoms = $input['symptoms'];

		// validasi dulu inputan dari formnya
		$v = Validator::make($input,['name'=>'required']);
		if($v->fails()) return json_encode(['status'=>'error','message'=>$v->errors()]);

		// Jika id > 0, maka merupakan form update, tapi klo kurang dari atau sama dengan 0 maka tambah
		if($input['id']>0){
		// 	// kode update
		// 	$ruleitem       = Rule::find($input['id']);
		// 	$ruleitem->name = $input['name'];
		// 	$ruleitem->save();
		// 	$idnya          = $input['id'];
		}else{
			// Masukkan ke tabel rule
			$idnya = DB::transaction(function() {
				$rule              = new Rule;
				$rule->ruleitem_id = Input::get('name');
				$rule->cf = Input::get('cf');
				$rule->save();
				// Masukkan ke tabel symptoms
				foreach(Input::get('symptoms') as $s):
					$symptom              = new Symptom;
					$symptom->rule_id     = $rule->id;
					$symptom->ruleitem_id = $s;
					$symptom->save();
				endforeach;
				return $rule->id;
			});
		}
		// get data for the rule
		$data = Rule::find($idnya);
		return json_encode([
			'status'  => 'success',
			'message' => 'Successfully store the Rule',
			'payload' => ['id'=>$input['name'],'name'=>$data->ruleitem->name]
		]);
	}

	// api untuk menyimpan data symptom
	public function postUpdate()
	{
		// ambil inputnya
		$input = Input::only(['name','idRule']);
		
		// validasi dulu inputan dari formnya
		$v = Validator::make($input,['name'=>'required','idRule']);
		if($v->fails()) return json_encode(['status'=>'error','message'=>'Gagal Update Data Rule']);

		// Masukkan datanya ke database
		$symptom = Rule::find($input['idRule']);
		$symptom->symptom = $input['name'];
		$symptom->save();

		// ambil data dari yang berhasil diinput tadi
		$symptom = Rule::with('Ruleitem')->find($symptom->id);
		return json_encode([
			'status'=>'success',
			'message'=>'Berhasil Update Data Rule',
			'payload'=>['id'=>$input['idRule'],'symptom'=>$input['name']]
		]);
	}

	// api untuk menghapus data symptom
	public function getDelete($id)
	{
		$symptom = Rule::find($id);
		$symptom->delete();
	}
}