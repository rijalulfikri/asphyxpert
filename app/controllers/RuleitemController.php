<?php
class RuleitemController extends BaseController {

	// layout utama yang bakal digunakan untuk seluruh view
	protected $layout = 'layouts.main';

	public function getIndex()
	{
		$this->layout->content = View::make('ruleitem.index');
	}

	// api untuk ambil seluruh penyakit
	public function getAll(){
		return Ruleitem::all();
	}

	// api untuk ambil satu penyakit berdasarkan id
	public function getFind($id){
		return Ruleitem::find($id);
	}

	// api untuk menyimpan data symptom
	public function postStore()
	{
		// ambil inputnya
		$input = Input::only(['id','name']);
		// validasi dulu inputan dari formnya
		$v = Validator::make($input,['name'=>'required']);
		if($v->fails()) return json_encode(['status'=>'error','message'=>$v->errors()]);

		// Jika id > 0, maka merupakan form update, tapi klo kurang dari atau sama dengan 0 maka tambah
		if($input['id']>0){
			// kode update
			$ruleitem       = Ruleitem::find($input['id']);
			$ruleitem->name = $input['name'];
			$ruleitem->save();
			$idnya          = $input['id'];
		}else{
			// kode tambah
			$ruleitem       = new Ruleitem;
			$ruleitem->name = $input['name'];
			$ruleitem->save();
			$idnya          = $ruleitem->id;
		}

		return json_encode([
			'status'  => 'success',
			'message' => 'Successfully store the Rule Item',
			'payload' => ['id'=>$idnya,'name'=>$input['name']]
		]);
	}

	// api untuk menyimpan data symptom
	public function postUpdate()
	{
		// ambil inputnya
		$input = Input::only(['name','idRuleitem']);
		
		// validasi dulu inputan dari formnya
		$v = Validator::make($input,['name'=>'required','idRuleitem']);
		if($v->fails()) return json_encode(['status'=>'error','message'=>'Gagal Update Data Ruleitem']);

		// Masukkan datanya ke database
		$symptom = Ruleitem::find($input['idRuleitem']);
		$symptom->symptom = $input['name'];
		$symptom->save();

		// ambil data dari yang berhasil diinput tadi
		$symptom = Ruleitem::find($symptom->id);
		return json_encode([
			'status'=>'success',
			'message'=>'Berhasil Update Data Ruleitem',
			'payload'=>['id'=>$input['idRuleitem'],'symptom'=>$input['name']]
		]);
	}

	// api untuk menghapus data symptom
	public function getDelete($id)
	{
		$symptom = Ruleitem::find($id);
		$symptom->delete();
	}
}