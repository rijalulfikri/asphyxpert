<?php
class DiseaseController extends BaseController {

	// layout utama yang bakal digunakan untuk seluruh view
	protected $layout = 'layouts.main';

	public function getIndex()
	{
		// tampilkan view index nya
		$this->layout->content = View::make('disease.index');
	}

	// api untuk ambil seluruh penyakit
	public function getDisease(){
		return Disease::all();
	}
	public function postStore()
	{
		// ambil inputnya
		$input = Input::all();
		// validasi dulu inputan dari formnya
		$v = Validator::make($input,['name'=>'required','symptomData'=>'required']);
		if($v->fails()) return json_encode(['status'=>'error','message'=>$v->messages]);

		// Input data penyakitnya
		$d = new Disease;
		$d->name = $input['name'];
		$d->save();
		// Input data gejala

	}
}