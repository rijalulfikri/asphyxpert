<?php
class DiagnoseController extends BaseController {

	// layout utama yang bakal digunakan untuk seluruh view
	protected $layout       = 'layouts.main';
	protected $facts        = [];
	protected $wm           = []; // working memory dalam format yang lebih manusiawi
	protected $rules        = [];
	protected $invalidRules = [];
	protected $facts_ids    = [];

	public function getIndex()
	{
		$this->layout->content = View::make('diagnose.index');
	}

	// API untuk Diagnosa Penyakit
	public function postDiagnose(){
		// Initialize vars
		$CFIndividuals = [];
		// Terima inputan facts
		// Inputan fact akan berupa json, yang berisi id dan cf nya
		// [{id:...,cf:...},...,{id:...,cf:...}]
		$input       = Input::only('facts');
		$this->facts = $input['facts'];

		// Pisahkan ID dan CF untuk kebutuhan query cari rule berkaitan
		foreach($input['facts'] as $i):
			$this->wm[$i['id']] = $i['cf'];
			$this->facts_ids[] = $i['id'];
		endforeach;

		// Validate
		if(count($input) <= 0) return json_encode(['status'=>'error','message'=>'You have to provide symptoms to diagnose']);

		// Ambil seluruh rule yang mempunyai symptom sesuai dengan facts
		$getRuleIds = Symptom::with('rule')
					->wherein('ruleitem_id',$this->facts_ids)
					->groupBy('rule_id')
					->get()->toArray();
		$this->rules = $getRuleIds;
		// Contoh output [{'rule_id':1},{'rule_id':2},...,{'rule_id':n}]
		
		// Cek valid atau tidak rule tersebut
		for($i=0;$i<count($this->rules);$i++):
			$this->validateRule($this->rules[$i]['rule_id'],$i);
		endfor;
		// return (json_encode($this->rules));

		/*
			Untuk menghitung CF harus diketahui dulu nama penyakitnya
			data yang kita punya adalah rule nya. 2 Rule bisa memiliki 1 penyakit (ruleitem) yang sama
			Jadi looping semua rule, ambil penyakitnya, kalau sama gabungin jadi 1
		*/
		$penyakit = [];
		$temp_id  = [];
		for($i=0;$i<count($this->rules);$i++):
			if(!in_array($this->rules[$i]['rule']['ruleitem_id'],$temp_id,true)):
				$temp_id[]                                         = $this->rules[$i]['rule']['ruleitem_id'];
				$penyakit[$this->rules[$i]['rule']['ruleitem_id']] = $this->rules[$i]['rule']['cf'];
			endif;
		endfor;

		/*
			Setelah berhasil mengelompokkan penyakit, ambil gejala2 dari penyakit tersebut
			- Cari di tabel symptom gejala2 dari penyakit itu
			- Hitung CF nya
		*/
		$CFCombine = [];
		$cfTemp = 0;
		foreach($penyakit as $k=>$v):
			// ambil tiap rule dari penyakit ini
			$rule = Rule::with('symptoms')->where('ruleitem_id','=',$k)->get()->toArray();
			$i=1;
			foreach($rule as $r):
				// Ambil symptomnya dan hitung CF nya
				// ambil juga minimum cf dari seluruh symptomnya

				$ambilcfsymptom = Symptom::where('rule_id','=',$r['id'])
									->get(['ruleitem_id'])->toArray();
				$min = [1];
				// print_r($ambilcfsymptom);
				// echo "<br>";
				// return $this->wm;
				foreach($ambilcfsymptom as $s):
					// cek dulu ada atau tidak di working memory cf dari symptom ini
					// klo ga ada, ada kemungkinan ini juga merupakan rule
					// jalankan fungsi untuk cari cf nya kemudian push lagi ke working memory
					if(!in_array($s['ruleitem_id'],array_keys($this->wm),true)){
						$cfnya = $this->calculateCF($s['ruleitem_id']);
					}else{
						$cfnya = $this->wm[$s['ruleitem_id']];
					}
					$min = min($min,$cfnya);
				endforeach;
				// Setelah dapat semua nilai minimum, cari nilai CF1 nya
				$cfTemp = $min*$penyakit[$k];
				if($i==1):
					$CFCombine[$k] = $cfTemp;
				else:
					// CFCombine = CF1 + CF2 - (CF1 x CF2)
					$CFCombine[$k] = $CFCombine[$k] + $cfTemp - ($CFCombine[$k] * $cfTemp);
				endif;
				$i++;
			endforeach;
		endforeach;
		// setelah dapet CFCombine, siapin data untuk dikirim ke client
		$data = [];
		foreach($CFCombine as $k=>$v):
			$s = Rule::with('symptoms')->where('ruleitem_id','=',$k)->get()->toArray();
			foreach($s as $s):
				$data[$k] = [
					'id' => $k,
					'cf' => $v,
				];
				$data[$k]['symptoms'] = $s['symptoms'];
			endforeach;
		endforeach;
		return json_encode($data);
	}

	// Fungsi untuk mendapatkan Symptom
	public function getSymptom($rule_id){
		$symptom = Symptom::where('rule_id','=',$rule_id)
						->get(['ruleitem_id'])->toArray();
	}

	// Fungsi untuk menghitung CF Individual
	public function calculateCF($rule_id){
		// check dulu di rule, ado ruleitem_id apo idak, klo ado ambek datanyo di rule
		$check = Rule::where('ruleitem_id','=',$rule_id)->first();
		if(count($check)<1):
			// berarti ini bukan rule, buangke bae, jadike 0
			$theCF = 0;
			return $theCF;
		else:
			// ternyato dio rule, ambek gejalanyo
			$symptom = Symptom::where('rule_id','=',$check['id'])
						->get(['ruleitem_id'])->toArray();
			// udah dapet gejalanyo, looping, pastike seluruh gejala ado di wm, klo katek
			// ulangi lagi berarti sampe abis, dak ado di wm jadike 0
			$cfnyo = 1;
			foreach($symptom as $s):
				if(!in_array($s['ruleitem_id'], array_keys($this->wm),true)):
					$cfnyo = $this->calculateCF($check['id']);
				else:
					// itung cf nyo
					$cfnyo = min($cfnyo,$this->wm[$s['ruleitem_id']]);
				endif;
			endforeach;
			return $theCF = $cfnyo * $check['cf'];
		endif;
	}
	// Fungsi inferensi
	public function validateRule($rule_id,$i){
		// Ambil gejala2 dari si rule
		$symptoms = $this->getSymptom($rule_id);
		/*
			Looping tiap gejala, pastikan ada didalam fact
			Seandainya pun tidak ada, cek di tabel rules, siapa tau 
			merupakan konklusi dari rule lain
		*/
		$jmlSymptom = count($symptoms);
		for($j=0;$j<$jmlSymptom;$j++):
			// Check apakah si symptom ini ada dalam fact
			if(!in_array($symptoms[$j]['ruleitem_id'], $this->facts_ids, true)):
				// Check ke tabel rules, apakah ia disease atau bukan
				$checkRule = Rule::where('ruleitem_id','=',$symptoms[$j])->first();
				if(count($checkRule)<1):
					unset($this->rules[$i]);
					$j = $jmlSymptom;
					continue;
				else:
					$this->validateRule($checkRule->id,$i);
				endif;
			endif;
		endfor;
	}
}