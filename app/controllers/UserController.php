<?php

class UserController extends BaseController {

	public function getLogin()
	{
		// kembalikan View untuk halaman login
		return View::make('login');
	}

	public function postLogin()
	{
		// Proses data login
		// Ambil input dari halaman login
		$input = Input::only(['email','password']); 

		// Kita validasi sesuai rule nya (ga boleh kosong)
		$v = Validator::make($input, [
			'email'=>'required|email',
			'password'=>'required'
		]);
		// Jika validasi gagal, kembalikan ke halaman sebelumnya, dan tampilkan pesan error
		if($v->fails()) return Redirect::back()->withErrors($v);
		// Klo berhasil, kita jalankan fungsi untuk otentikasi, klo pass masuk aplikasi, klo gagal balik lagi ke halaman login
		if(Auth::attempt($input))
		{
			return Redirect::intended('ruleitems');
		}
		else
		{
			Session::flash('error','Incorrect Login Credential');
			return Redirect::back();
		}
	}

	// Ini fungsi untuk logout
	public function getLogout()
	{
		Auth::logout();
		Session::flash('success','Successfully Logout');
		return Redirect::route('home');
	}
}