//= include ../packages/angular/angular.js
//= include ../packages/angular-animate/angular-animate.min.js
//= include ../packages/angular-ui-bootstrap/src/transition/transition.js
//= include ../packages/angular-ui-bootstrap/src/collapse/collapse.js
//= include ../packages/angular-ui-bootstrap/src/position/position.js
//= include ../packages/angular-ui-bootstrap/src/bindHtml/bindHtml.js
//= include ../packages/angular-ui-bootstrap/src/typeahead/typeahead.js
//= include ../packages/angular-ui-bootstrap/src/modal/modal.js
//= include ../packages/amcharts/dist/amcharts/amcharts.js
//= include ../packages/amcharts/dist/amcharts/serial.js
//= include ../packages/amcharts/dist/amcharts/themes/dark.js

angular.module('asphyxpert',['ngAnimate','ui.bootstrap.modal','ui.bootstrap.typeahead','ui.bootstrap.collapse']);
//= include controller/ruleitemController.js
//= include controller/ruleController.js
//= include controller/rulePopupController.js
//= include controller/diagnoseController.js