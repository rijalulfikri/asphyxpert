angular.module('asphyxpert')
	.controller('ruleitemController',['$scope','$http',function($scope,$http){
		$scope.isCollapsed    = true; // sliding form tambah
		// Initialize main ruleitem variable as an object and initialize basic id and edit text
		$scope.ruleitem       = {};
		$scope.ruleitem.id    = 0;
		$scope.ruleitem.index = 0;
		$scope.ruleitem.name  = '';
		$scope.ruleitems      = [{id:0,name:'Loading ...'}];

		// API untuk ambil data ruleitem
		var urlAll = baseUrl+'/ruleitems';
		$scope.ruleitem.loadAllRuleitem = function(){
			$http.get(urlAll).success(function(data){
				$scope.ruleitems = data;
			});
		};
		$scope.ruleitem.loadAllRuleitem();
		
		// API untuk store data ruleitem
		$scope.ruleitem.storeRuleitem = function(){
			// menentukan alamat API yang bertanggung jawab untuk store data (update+create) 
			var urlStore = baseUrl+'/ruleitem/store';
			// Initialize data needed to be sent
			var theId = parseInt($scope.ruleitem.id);
			var theData = {'id':theId,'name':$scope.ruleitem.name};
			// Access API tadi, jika berhasil maka tambahkan data baru tadi pada list rule item
			$http.post(urlStore,theData).success(function(data){
				if(theId>0){
					$scope.ruleitems[$scope.ruleitem.index].name = data.payload.name;
					$scope.ruleitem.editState = false;
				}else{
					$scope.ruleitems.push(data.payload);
				}
				$scope.ruleitem.id    = 0;
				$scope.ruleitem.index = 0;
				$scope.ruleitem.name  = '';
				$scope.isCollapsed    = true;
			});
		}

		// Fungsi sewaktu tombol edit di click
		$scope.editRuleitem = function(id,name,index){
			// Munculkan form
			$scope.isCollapsed = false;
			// Ubah variabel id agar menyimpan id sesuai dengan item yang di click
			$scope.ruleitem.id    = id;
			$scope.ruleitem.name  = name;
			$scope.ruleitem.index = index;
			$scope.ruleitem.editState = true;
		}

		// Fungsi reset state untuk edit
		$scope.ruleitem.resetState = function(){
			$scope.ruleitem.name      = ''; // kosongkan textfield
			$scope.ruleitem.id        = 0;  // kosongkan id
			$scope.ruleitem.editState = false; // buang tombol cancel
			$scope.isCollapsed        = true; // form slide up
		}
		// Get ruleitem by ID
		$scope.ruleitem.findRuleitem = function(id){
			$http.get(baseUrl+'/ruleitem/'+id)
				.success(function(data){
					$scope.ruleitem.id   = data.id;
					$scope.ruleitem.name = data.ruleitem;
				});
		};

		// Delete data function
		$scope.deleteRuleitem = function(id,idArr){
			if(confirm('Are you sure you want to delete this data?')){
				$http.get(baseUrl+'/ruleitem/'+id+'/delete')
					.success(function(data){
						$scope.ruleitems.splice(idArr,1);
					});
			}
		};

		// Clear data after editing
		$scope.ruleitem.clearData = function(){
			$scope.ruleitem.id   = 0;
			$scope.ruleitem.name = '';
		};
	}]);