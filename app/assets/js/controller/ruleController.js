angular.module('asphyxpert')
	.controller('ruleController',['$scope','$http','$modal',function($scope,$http,$modal){
		$scope.isCollapsed    = true; // sliding form tambah
		// Initialize main ruleitem variable as an object and initialize basic id and edit text
		$scope.rule           = {};
		$scope.rule.symptoms  = [{}];
		$scope.rule.name      = '';
		$scope.ruleitems       = {};
		$scope.rulelists       = [];
		$scope.symptoms = [];
		// $scope.rules.id    = 0;
		// $scope.rules.index = 0;
		// Load seluruh rules
		var urlAllRule = baseUrl+'/rules';
		$scope.ruleitems.loadAllRules = function(){
			$http.get(urlAllRule).success(function(data){
				angular.forEach(data,function(d){
					$scope.rulelists.push({id:d.id,name:d.ruleitem.name});
				});
			}).error(function(data){
				console.log(data);
			});
		};
		$scope.ruleitems.loadAllRules();
		// API untuk ambil data ruleitem
		var urlAll = baseUrl+'/ruleitems';
		$scope.ruleitems.loadAllRuleitem = function(){
			$http.get(urlAll).success(function(data){
				$scope.ruleitems.symptoms = data;
				$scope.rule.symptoms = data;
			}).error(function(data){
				console.log(data);
			});
		};
		$scope.ruleitems.loadAllRuleitem();
		// API untuk store data ruleitem
		$scope.rule.storeRule = function(){
			// menentukan alamat API yang bertanggung jawab untuk store data (update+create) 
			var urlStore = baseUrl+'/rule/store';
			// Initialize data needed to be sent
			var symptoms = [];
			angular.forEach($scope.rule.symptoms, function(r) {
		      // if (r.checked) symptoms.push({'id':r.id,'cf':r.cf});
		      if (r.checked) symptoms.push(r.id);
		    });
			var theId = parseInt($scope.rule.id);
			var theData = {'id':theId,'name':$scope.rule.name.id,'cf':$scope.rule.cf,'symptoms':symptoms};
			// Access API tadi, jika berhasil maka tambahkan data baru tadi pada list rule item
			$http.post(urlStore,theData).success(function(data){
				if(theId>0){
					// $scope.rule[$scope.rule.index].name = data.payload.name;
				}else{
					$scope.rulelists.push(data.payload);
				}
				console.log($scope.rulelists);
				// $scope.rule.id    = 0;
				// $scope.rule.index = 0;
				// $scope.rule.name  = '';
				$scope.isCollapsed    = true;
				$scope.ruleitems.loadAllRuleitem();
			});
		}

		// Munculin popup ketika Rule di click yang isinya symptom2 nya
		$scope.showSymptoms = function (id) {
			var modalInstance = $modal.open({
				templateUrl: baseUri+'/js/partials/rulePopup.html',
				controller: 'rulePopupController',
				resolve: {
					id: function(){
						return id;
					}
				}
			});
		};

		// Fungsi sewaktu tombol edit di click
		$scope.editRuleitem = function(id,name,index){
			// Munculkan form
			$scope.isCollapsed = false;
			// Ubah variabel id agar menyimpan id sesuai dengan item yang di click
			$scope.ruleitems.id    = id;
			$scope.ruleitems.name  = name;
			$scope.ruleitems.index = index;
		}
		// Get ruleitem by ID
		$scope.ruleitems.findRuleitem = function(id){
			$http.get(baseUrl+'/ruleitem/'+id)
				.success(function(data){
					$scope.ruleitems.id   = data.id;
					$scope.ruleitems.name = data.ruleitem;
				});
		};

		// Delete data function
		$scope.deleteRuleitem = function(id,idArr){
			if(confirm('Are you sure you want to delete this data?')){
				$http.get(baseUrl+'/ruleitem/'+id+'/delete')
					.success(function(data){
						$scope.ruleitems.splice(idArr,1);
					});
			}
		};

		// Clear data after editing
		$scope.ruleitems.clearData = function(){
			$scope.ruleitems.id   = 0;
			$scope.ruleitems.name = '';
		};
	}]);