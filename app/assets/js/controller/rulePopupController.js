angular.module('asphyxpert')
	.controller('rulePopupController',['$scope','$http','id',function($scope,$http,id){
		$scope.symptoms = [];
		$scope.isLoaded = false;
		$scope.loadSymptoms = function(){
			$http.get(baseUrl+'/rule/'+id+'/symptom').success(function(data){
				$scope.symptoms = data;
				$scope.isLoaded = true;
			}).error(function(data){
				console.log(data);
			});
		};
		$scope.loadSymptoms();
	}]);