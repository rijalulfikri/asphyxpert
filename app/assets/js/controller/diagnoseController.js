angular.module('asphyxpert')
	.controller('diagnoseController',['$scope','$http',function($scope,$http){
		$scope.inputs = [];
		$scope.facts = [];
		$scope.isResult = false;
		$scope.symptomlist = [];
		$scope.data = [];
		// Loading Seluruh Rule Items
		var urlAll = baseUrl+'/ruleitems';
		$scope.loadAllRuleitem = function(){
			$http.get(urlAll).success(function(data){
				$scope.ruleitems = data;
				$scope.inputs = data;
				console.log($scope.inputs);
				angular.forEach(data,function(d){
					$scope.symptomlist[d.id] = d.name;
					// $scope.diagnose.item.push({'a':d.name});
				});
			});
		};
		$scope.loadAllRuleitem();
		$scope.diagnose = function(){
			$scope.facts = [];
			angular.forEach($scope.inputs,function(input){
				if(input.checked) $scope.facts.push({'id':input.id,'cf':input.cf});
			});
			$http.post(baseUrl+'/diagnose',{'facts':$scope.facts}).success(function(data){
				$scope.data = data;
				var dataProvider = [];
				angular.forEach(data, function(d){
					dataProvider.push({
						'category':$scope.symptomlist[d.id],
						'column-1':d.cf
					});
				});
				// data untuk chart
				$scope.chartData = {
					"type": "serial",
					"pathToImages": "http://cdn.amcharts.com/lib/3/images/",
					"categoryField": "category",
					"startDuration": 1,
					"theme": "dark",
					"categoryAxis": {
						"gridPosition": "start"
					},
					"trendLines": [],
					"graphs": [
						{
							"colorField": "color",
							"fillAlphas": 1,
							"id": "AmGraph-1",
							"lineColorField": "color",
							"title": "graph 1",
							"type": "column",
							"valueField": "column-1"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"title": "CF Value"
						}
					],
					"allLabels": [],
					"balloon": {},
					"titles": [
						{
							"id": "Title-1",
							"size": 15,
							"text": "Grafik Diagnosa Penyakit"
						}
					],
					"dataProvider": dataProvider
				}
				AmCharts.makeChart('chartdiv',$scope.chartData);
				$scope.isResult = true;
				// console.log($scope.data);
			}).error(function(data){
				console.log('Gagal Post Data Untuk Diagnose, berikut errornya:');
				console.log(data);
			});
		};
	}]);