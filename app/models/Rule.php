<?php

class Rule extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'rules';

	public function symptoms(){
		return $this->hasMany('Symptom');
	}

	public function ruleitem(){
		return $this->belongsTo('Ruleitem','ruleitem_id');
	}
}
