<?php

class Symptom extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'symptoms';

	public function rule(){
		return $this->belongsTo('Rule');
	}

	public function ruleitem(){
		return $this->belongsTo('Ruleitem');
	}
}
