<?php

class Disease extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'diseases';

	static $rules = ['name'=>'required','description'=>'required'];

	public function symptoms(){
		return $this->hasMany('Symptom');
	}
}
