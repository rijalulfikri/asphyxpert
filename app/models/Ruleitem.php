<?php

class Ruleitem extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'ruleitems';

	public function rule(){
		return $this->belongsTo('Rule');
	}

	public function symptom(){
		return $this->belongsTo('Symptoms','ruleitem_id');
	}
}
