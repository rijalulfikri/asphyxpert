<?php

class UserTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('users')->truncate();
		User::create([
			'email'=>'fikri.desertlion@gmail.com',
			'password'=>Hash::make('123456')
		]);
	}

}
